/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week6.MockingExperiment;

/**
 *
 * @author rod
 */
public class Bar {
    
    private boolean isSpecial;
    private String specialString;
    
    /**
     * For the sake of this example, imagine that Bar is a class that involves
     * a complicated setup procedure, such as reading data from a database
     * or from a set of files, before it can be used by Foo
     */
    
    public boolean isSpecial() {
        return isSpecial;
    }
    
    public String specialString() {
        return specialString;
    }
    public String toString() {
        return "";
    }
}
