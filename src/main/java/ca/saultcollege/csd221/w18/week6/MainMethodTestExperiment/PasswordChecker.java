/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week6.MainMethodTestExperiment;

import java.util.List;

/**
 *
 * @author rod
 */
public class PasswordChecker {
    
    /**
     * Verify that a password has both uppercase, lowercase, numeric, and
     * special symbols, and that it is at least 8 characters long.
     * @param password
     * @return True if the password is acceptable, false otherwise
     */
    public boolean check(String password) throws SecurityException {
        
        if ( password.length() < 8 ) { return false; }
        
        if ( password.equals("password") ) {
            throw new SecurityException(
                    "This user should not be allowed to use computers");
        }
        
        boolean hasUpperCaseChar = false;
        boolean hasLowerCaseChar = false;
        boolean hasNumberChar = false;
        boolean hasSpecialChar = false;
        
        for ( char c : password.toCharArray() ) {
            
            if ( 'A' <= c && c <= 'Z' ) { hasUpperCaseChar = true; }
            if ( 'a' <= c && c <= 'z' ) { hasLowerCaseChar = true; }
            if ( '0' <= c && c <= '9' ) { hasNumberChar = true; }
            
            if ( ! hasSpecialChar ) {
                for ( char s : " .,!@#$%^&*()_-=+?<> ".toCharArray() ) {
                    if ( s == c ) {
                        hasSpecialChar = true;
                        break;
                    }
                }
            }
            
            if ( hasUpperCaseChar && hasLowerCaseChar && hasNumberChar &&
                    hasSpecialChar )
            {
                return true;
            }
        }
        
        return false;
    }
    
    public class SecurityException extends RuntimeException {
        public SecurityException(String msg) { super(msg); }
    }
    
    public static void main(String[] args) {
        
        try {
            // Test that empty string fails
            PasswordChecker checker = new PasswordChecker();
            if ( checker.check("") ) {
                System.err.println("FAIL: '' is not valid");
            }
            
            // Test that valid password passes
            if ( ! checker.check("Abc123!!") ) {
                System.err.println("FAIL: valid password passes");
            }
            
            // Test that password with no uppercase chars fails
            if ( checker.check("abc123!!") ) {
                System.err.println("FAIL: passwords must have one uppercase char");
            }
            
            // Test that password with no lowercase chars fails
            if ( checker.check("ABC123!!") ) {
                System.err.println("FAIL: passwords must have one lowercase char");
            }
            
            // Test that password with no number chars fails
            if ( checker.check("AbcAbc!!") ) {
                System.err.println("FAIL: passwords must have one number char");
            }
            
            // Test that password with no special chars fails
            if ( checker.check("Abc123Abc") ) {
                System.err.println("FAIL: passwords must have one special char");
            }

            // Test that 'password' throws exception
            checker.check("password");
            System.err.println("FAIL: SecurityException should throw on 'password'");
        } catch ( SecurityException e ) {
            
        }

    }
}
