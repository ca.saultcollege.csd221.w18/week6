/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week6.MockingExperiment;

/**
 *
 * @author rod
 */
public class Foo {
    
    public String bar(Bar b) {
        if ( b.isSpecial() ) { return b.specialString(); }
        else { return b.toString(); }
    }
    
}
