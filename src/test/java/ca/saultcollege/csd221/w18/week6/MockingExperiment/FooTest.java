/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week6.MockingExperiment;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rod
 */
public class FooTest {

    @Test
    public void testThatBarReturnsSpecialStringIfBarIsSpecial() {
        Bar b = null; // What could we do here so that we don’t have to go 
                      // through the complicated process of making a b object, 
                      // and so that the assert below passes?
                      
        Foo f = new Foo();
        assertEquals("special string", f.bar(b));
    }
    
}
