/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week6.MainMethodTestExperiment;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

import ca.saultcollege.csd221.w18.week6.MainMethodTestExperiment.PasswordChecker.SecurityException;

/**
 *
 * @author rod
 */
public class PasswordCheckerTest {
    
    PasswordChecker checker;
    
    @Before
    public void setup() {
        checker = new PasswordChecker();
    }

    @Test
    public void testThatEmptyStringFails() {
        assertFalse("Should return false for blank password", 
                checker.check(""));
    }
    
    @Test
    public void testThatValidPasswordPasses() {
        assertTrue("Should return true for valid password", 
                checker.check("Abc123!!"));
    }
            
    @Test
    public void testThatPasswordWithoutUppercaseCharsFails() {
        assertFalse("Should reject passwords with no uppercase chars", 
                checker.check("abc123!!"));
    }
    
    @Test
    public void testThatPasswordWithoutLowercaseCharsFails() {
        assertFalse("Should reject passwords with no lowercase chars", 
                checker.check("ABC123!!"));
    }
    
    @Test
    public void testThatPasswordWithoutNumericCharsFails() {
        assertFalse("Should reject passwords with no numeric chars", 
                checker.check("abcABC!!"));
    }
    
    @Test
    public void testThatPasswordWithoutSpecialCharsFails() {
        assertFalse("Should reject passwords with no special chars",
                checker.check("ABCabc123"));
    }
    
    @Test(expected=SecurityException.class)
    public void testThatStupidPasswordThrowsException() {
        checker.check("password");
    }

}
